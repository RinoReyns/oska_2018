package com.example.rinoreyns.oska_2018;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private SeekBar seek_bar;
    private TextView text_View;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekbarr();
    }

    public void seekbarr()
    {
        seek_bar=findViewById(R.id.slider);
        text_View=findViewById(R.id.textView2);
        text_View.setText("slider value: "+seek_bar.getProgress()+"/ "+seek_bar.getMax());

        seek_bar.setOnSeekBarChangeListener(
                new SeekBar.OnSeekBarChangeListener() {
                    int progress_value;
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        progress_value=progress;
                        text_View.setText("slider value: "+progress+"/ "+seek_bar.getMax());
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        text_View.setText("slider value: "+progress_value+"/ "+seek_bar.getMax());

                    }
                }
        );

    }
}
